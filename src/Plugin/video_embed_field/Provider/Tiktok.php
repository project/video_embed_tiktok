<?php

namespace Drupal\video_embed_tiktok\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A Tiktok video provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "tiktok",
 *   title = @Translation("Tiktok")
 * )
 */
class Tiktok extends ProviderPluginBase {

  /**
   * Tiktok oembed data.
   *
   * @var array|null
   */
  protected $oEmbedData = NULL;

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => 'tiktok',
      '#url' => sprintf('https://www.tiktok.com/embed/%s', $this->getVideoId()),
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function oembedData() {
    if (!isset($this->oembedData)) {
      $this->oEmbedData = json_decode(file_get_contents(sprintf('https://www.tiktok.com/oembed?url=%s', $this->getInput())), TRUE);
    }
    return $this->oEmbedData;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return $this->oembedData()['thumbnail_url'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    if (preg_match('/https?:\/\/(www\.)?tiktok.com\/(?<user_id>@[\S]*)\/video\/(?<id>[0-9]*)\/?/', $input, $matches)) {
      return $matches['id'] ?? FALSE;
    }
  }

}
