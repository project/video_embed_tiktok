CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * FAQ
 * Maintainers


INTRODUCTION
------------

Tiktok Video Embed Field is a submodule of Video Embed Field for Tiktok Video
support.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/video_embed_tiktok

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/video_embed_tiktok


REQUIREMENTS
------------

 * Video Embed Field (https://www.drupal.org/project/video_embed_field):
   Video Embed field creates a simple field type that allows you to embed videos
   from YouTube and Vimeo and show their thumbnail previews simply by entering
   the video's url.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

The configuration is under the field type video_embed_field. It appears in 
the list of allowed providers. If you have restricted the allowed providers 
you will need to specifically add the "Tiktok" provider to the list of allowed 
providers in your field.


USAGE
-----

 * Install module
 * Add video embed field and enable "Tiktok" provider
 * Add Tiktok link

FAQ
---

None yet. If you’ve got one, contact the maintainers or create a issue.


MAINTAINERS
-----------

Current maintainers:
 * Rahul Rasgon (https://www.drupal.org/u/rahulrasgon)
 
 This project has been sponsored by:
 * QED42
  QED42 is a web development agency focused on helping organizations and
  individuals reach their potential, most of our work is in the space of
  publishing, e-commerce, social and enterprise.
