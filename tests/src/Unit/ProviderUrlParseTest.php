<?php

namespace Drupal\Tests\video_embed_tiktok\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\video_embed_tiktok\Plugin\video_embed_field\Provider\Tiktok;

/**
 * Test that URL parsing for the provider is functioning.
 *
 * @group video_embed_tiktok
 */
class ProviderUrlParseTest extends UnitTestCase {

  /**
   * Test URL parsing works as expected.
   *
   * @dataProvider urlsWithExpectedIds
   */
  public function testUrlParsing($url, $expected) {
    $this->assertEquals($expected, Tiktok::getIdFromInput($url));
  }

  /**
   * A data provider for URL parsing test cases.
   *
   * @return array
   *   An array of test cases.
   */
  public function urlsWithExpectedIds() {
    return [
      [
        'https://www.tiktok.com/@scout2015/video/6718335390845095173/',
        '6718335390845095173',
      ],
      [
        'http://www.tiktok.com/@scout2015/video/6718335390845095173/',
        '6718335390845095173',
      ],
      [
        'http://www.tiktok.com/@scout2015/video/6718335390845095173',
        '6718335390845095173',
      ],
      [
        'https://www.tiktok.com/@scout2015/video/6718335390845095173',
        '6718335390845095173',
      ],
      [
        'https://www.tiktok.com/@scout2015/video/6718335390845095173/?taken-by=tiktok',
        '6718335390845095173',
      ],
    ];
  }

}
